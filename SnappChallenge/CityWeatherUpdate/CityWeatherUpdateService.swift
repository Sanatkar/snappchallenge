//
//  CityWeatherUpdateService.swift
//  SnappChallenge
//
//  Created by Ali Farhadi on 6/28/23.
//

import Foundation

final class CityWeatherUpdateService {
    
    static let shared = CityWeatherUpdateService()
    
    private init() { }
    
    func subscribe(city: City, listener: @escaping (CityWeather) -> Void) {
        let url = "https://api.openweathermap.org/data/2.5/weather?lat=\(city.coordinate.latitude)&lon=\(city.coordinate.longitude)&appid=36b049311cf0307e2083e14a072f6dd0"
        var request = URLRequest(url: URL(string: url)!)
        request.allHTTPHeaderFields = ["Content-Type": "application/json"]
        URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            listener(try! JSONDecoder().decode(CityWeather.self, from: data!))
        }).resume()
    }
}
