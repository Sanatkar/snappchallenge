//
//  CityWeather.swift
//  SnappChallenge
//
//  Created by Ali Farhadi on 6/28/23.
//

import Foundation

struct CityWeather: Codable {
    let id: Int
    var weather: [WeatherData]
    var main: MainData
}

struct WeatherData: Codable {
    var description: String?
}

struct MainData: Codable {
    var temp: Double?
}
