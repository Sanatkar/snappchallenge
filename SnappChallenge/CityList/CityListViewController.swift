//
//  CityListViewController.swift
//  SnappChallenge
//
//  Created by Ali Farhadi on 6/28/23.
//

import UIKit

class CityListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let cities: [City] = City.demoCities
    
    var weathers = [CityWeather]()

    let cityWeatherService = CityWeatherUpdateService.shared

    var tableView = UITableView()
    var maxTempLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        maxTempLabel.text = "Max temp"

        for city in cities {
            cityWeatherService.subscribe(city: city) { cityWeather in
                print(cityWeather)
                
                if let index = self.weathers.firstIndex(where: { $0.id == city.id }) {
                    self.weathers[index].main = cityWeather.main
                    self.weathers[index].weather = cityWeather.weather
                } else {
                    self.weathers.append(cityWeather)
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }

        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(CityListCell.self, forCellReuseIdentifier: "CityListCell")
        
        view.addSubview(maxTempLabel)
        view.addSubview(tableView)
        
        maxTempLabel.translatesAutoresizingMaskIntoConstraints = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            maxTempLabel.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            maxTempLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.topAnchor.constraint(equalTo: maxTempLabel.bottomAnchor, constant: 10),
        ])
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityListCell", for: indexPath) as! CityListCell
        
        cell.nameLabel.text = cities[indexPath.row].name
        let weather = weathers.first(where: { $0.id == cities[indexPath.row].id })
        let temp: String = "\(weather?.main.temp ?? 0.0)𝇈"
        let condition = weather?.weather.first?.description ?? ""
        cell.priceLabel.text = temp + " - " + condition
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        #if DEBUG
        print("selected", indexPath)
        #endif
    }
}
