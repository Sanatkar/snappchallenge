//
//  City.swift
//  SnappChallenge
//
//  Created by Ali Farhadi on 6/28/23.
//

import Foundation
import CoreLocation

class City: NSObject {
    var id: Int
    var coordinate: CLLocationCoordinate2D
    var name: String

    init(id: Int, coordinate: CLLocationCoordinate2D, name: String) {
        self.id = id
        self.coordinate = coordinate
        self.name = name
    }
}

extension City {
    static let demoCities: [City] =  [
        City(id: 6659521, coordinate: .init(latitude: 35.7219, longitude: 51.3347), name: "Tehran"),
        City(id: 124665, coordinate: .init(latitude: 36.2972, longitude: 59.6067), name: "Mashhad"),
        City(id: 115019, coordinate: .init(latitude: 29.5926, longitude: 52.5836), name: "Shiraz")
    ]
}
